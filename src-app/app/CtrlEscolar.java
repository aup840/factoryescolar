package app;
import java.util.ArrayList;
import java.util.Scanner;

import facts.Factory;
import facts.FormatFactory;
import fwk.Alumno;
import fwk.BaseDatos;

public class CtrlEscolar {
	private Scanner obj;
	
	public void procesar(){
		menu();
		obj = new Scanner(System.in);
		
		int opc = obj.nextInt();
		
		String tipo = "";
		switch (opc){
		case 1:
			tipo = "TXT";
			break;
		case 2:
			tipo = "CSV";
			break;
		case 3: 
			tipo = "XML";
			break;
		case 4: 
			tipo = "JSON";
			break;
		case 5:	System.exit(0);
			break;
		}		
		
		BaseDatos bd = new BaseDatos();
		bd.cargar();
		ArrayList<Alumno> alumnos = bd.getAlumnos();
		System.out.println("Procesando.....");
		Factory factory = new  FormatFactory(tipo);
		factory.formato(tipo, alumnos);

		
		
	}
	
	public void menu(){
		System.out.println("Seleccione la opcion a realizar ");
		System.out.println("1  Exportar datos en formato TXT ");
		System.out.println("2  Exportar datos en formato CSV ");
		System.out.println("3  Exportar datos en formato XML ");
		System.out.println("4  Exportar datos en formato JSON ");
		System.out.println("5  Salir ");
		System.out.println("Seleccione la opcion a realizar ");
	}
}