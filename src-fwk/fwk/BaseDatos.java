package fwk;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class BaseDatos {
private ArrayList<Alumno> alumnos;
	
	public void cargar(){
		System.out.println("Cargadon los datos de la base de datos.....");		
         alumnos = new ArrayList<>();
	        try {
	            String cadena = "";	           	           
	            @SuppressWarnings("resource")	            
				BufferedReader bf = new BufferedReader( new FileReader(System.getProperty("user.dir") + "\\alumnos.txt"));
	            System.out.println();
	            while ((cadena = bf.readLine())!=null) { //mientras haya algo que leer desde el archivo
	            	
	            	if (!cadena.toUpperCase().contains("EDAD")){
		                ArrayList<String> palabras = new ArrayList<>(Arrays.asList(cadena.split("\\|")));
		                if (palabras.size() == 8){ //validando que tenga el mismo tama�o de columnas
			                Alumno al = new Alumno();
			                al.setnControl(palabras.get(0));
			                al.setNombre(palabras.get(1));
			                al.setApPaterno(palabras.get(2));
			                al.setApMaterno(palabras.get(3));
			                al.setEdad(palabras.get(4));
			                al.setSexo(palabras.get(5));
			                al.setCarrera(palabras.get(6));
			                al.setSemestre(palabras.get(7));
			                alumnos.add(al);
		                }		                
	            	}
	            }	            	           
	        } catch (IOException ex) {
	            ex.getMessage();
	        }
		}
	
	public ArrayList<Alumno> getAlumnos(){
		return alumnos;
	}
}