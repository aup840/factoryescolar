package fwk;

public class Alumno {
	private String nControl;
	private String nombre;
	private String apPaterno;
	private String apMaterno;
	private String edad;
	private String sexo;
	private String carrera;
	private String semestre;
	
	public void setnControl(String nControl) {
		this.nControl = nControl;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}
	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}
	public String getnControl() {
		return nControl;
	}
	public String getNombre() {
		return nombre;
	}
	public String getApPaterno() {
		return apPaterno;
	}
	public String getApMaterno() {
		return apMaterno;
	}
	public String getEdad() {
		return edad;
	}
	public String getSexo() {
		return sexo;
	}
	public String getCarrera() {
		return carrera;
	}
	public String getSemestre() {
		return semestre;
	}
	public void setEdad(String edad) {
		this.edad = edad;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}
	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}
}
