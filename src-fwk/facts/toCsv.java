package facts;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import fwk.Alumno;

public class toCsv extends Format {

	@Override
	public void exportar(ArrayList<Alumno> alumnos) {		
	       try {      
	            FileWriter fw=new FileWriter("resultados.csv");
	            try (BufferedWriter bw = new BufferedWriter(fw)) {    
	            	char separador = (char) (44); //separador
	                for (Alumno al: alumnos){
	                    bw.write(al.getnControl() + separador + al.getNombre() + separador + al.getApPaterno() + separador + 
	                    		al.getApMaterno() + separador + al.getEdad() + separador + al.getSexo() + separador + al.getCarrera() + separador + al.getSemestre() );
	                    bw.newLine();
	                }
	                bw.flush();     // guarda los datos en el archivo
	                System.out.println();
	                System.out.println("Se ha creado el archivo resultados.csv exitosamente con " + alumnos.size() + "  registros");
	            }
	        }
	        catch (IOException e){
	            System.err.print ("Error en la crear o guardar el archivo");     
	        }        
	}
}