package facts;
public class FormatFactory extends Factory {
	public FormatFactory(String type) {
		super(type);
	}

	public Format createFactory(String type){
		if (type.equalsIgnoreCase("TXT")){
			return new toTxt();
		}
		else if (type.equalsIgnoreCase("CSV")){
			return new toCsv();
		}
		else if (type.equalsIgnoreCase("XML")){
			return new toXml();
		}
		else if (type.equalsIgnoreCase("JSON")){
			return new toJson();
		}
		return null;
	}
}
