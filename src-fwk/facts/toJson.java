package facts;
import fwk.Alumno;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class toJson extends Format{

	@Override
	public void exportar(ArrayList<Alumno> alumnos) {
		int reg = 0;		
	       try {      
	            FileWriter fw=new FileWriter("resultados.json");
	          
	            try (BufferedWriter bw = new BufferedWriter(fw)) {
	            	bw.write("{");
	            	bw.newLine();
	            	bw.write("\t \"Alumnos\": [");	            	
	                for (Alumno al: alumnos){	
	                	reg++;
	                	bw.newLine();
	                	if (reg == alumnos.size()){
	                		bw.write("{ \"NCONTROL\": \"" + al.getnControl() + "\" , \"NOMBRE\": \"" + al.getNombre() + "\" , \"APPATERNO\": \"" + al.getApPaterno() + "\" , \"APMATERNO\": \"" + al.getApMaterno() + "\" , \"EDAD\":" + al.getEdad() + ", \"SEXO\": \"" + al.getSexo() + "\" , \"CARRERA\": \"" + al.getCarrera() + "\", \"SEMESTRE\":" + al.getSemestre() + "}");	                	
	                	}
	                	else{
	                		bw.write("{ \"NCONTROL\": \"" + al.getnControl() + "\" , \"NOMBRE\": \"" + al.getNombre() + "\" , \"APPATERNO\": \"" + al.getApPaterno() + "\" , \"APMATERNO\": \"" + al.getApMaterno() + "\" , \"EDAD\":" + al.getEdad() + ", \"SEXO\": \"" + al.getSexo() + "\" , \"CARRERA\": \"" + al.getCarrera() + "\", \"SEMESTRE\":" + al.getSemestre() + "},");
	                	}
	                }
	                bw.write("]");
	                bw.write("}");
	                bw.flush();     // guarda los datos en el archivo
	                System.out.println();
	                System.out.println("Se ha creado el archivo resultados.JSON exitosamente con " + alumnos.size() + "  registros");
	            }
	        }
	        catch (IOException e){
	            System.err.print ("Error en la crear o guardar el archivo");     
	        }        
	}
}
