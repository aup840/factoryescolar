package facts;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import fwk.Alumno;

public class toXml extends Format{

	@Override
	public void exportar(ArrayList<Alumno> alumnos) {			
	       try {      
	            FileWriter fw=new FileWriter("resultados.xml");
	            String encabezado = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><Alumnos>";
	            
	            try (BufferedWriter bw = new BufferedWriter(fw)) {
	            	bw.write(encabezado);
	                for (Alumno al: alumnos){
	                    bw.write("<ALUMNO> <NCONTROL> " + al.getnControl() + " </NCONTROL> <NOMBRE>" + al.getNombre() + " </NOMBRE> <APPATERNO> " + al.getApPaterno() + " </APPATERNO> <APMATERNO> " + al.getApMaterno() + " </APMATERNO> <EDAD> " + al.getEdad() + " </EDAD> <SEXO> " + al.getSexo() + " </SEXO> <CARRERA> " + al.getCarrera() + " </CARRERA> <SEMESTRE> " + al.getSemestre() + " </SEMESTRE> </ALUMNO>" );
	                    bw.newLine();
	                }
	                bw.write("</Alumnos>");
	                bw.flush();     // guarda los datos en el archivo
	                System.out.println();
	                System.out.println("Se ha creado el archivo resultados.xml exitosamente con " + alumnos.size() + "  registros");
	            }
	        }
	        catch (IOException e){
	            System.err.print ("Error en la crear o guardar el archivo");     
	        }        
	}
}