package facts;

import java.util.ArrayList;

import fwk.Alumno;

public abstract class Factory {
	@SuppressWarnings("unused")
	private String type;
	
	public Factory(String type){
		this.type = type;
	}
	public abstract Format createFactory(String type );
	
	public void formato(String type, ArrayList<Alumno> alumnos ){		
		Format fftxt =  createFactory(type);
		fftxt.exportar(alumnos); 
	}
}
