package test;
import static org.junit.Assert.*;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test;
import fwk.Alumno;
import fwk.BaseDatos;

public class TestBD {
	BaseDatos bd;
	ArrayList<Alumno> alumnos;
	@Before
	public void setUp() throws Exception {
		bd = new BaseDatos();
		bd.cargar();
		alumnos = bd.getAlumnos();
	}

	@Test
	public void testCargar() {
		assertTrue(alumnos.size()>0);
	}

	@Test
	public void testGetAlumnos() {
		assertTrue(alumnos.get(0).getnControl().equals("M02171139"));
	}

}
