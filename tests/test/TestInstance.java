package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import facts.Format;
import facts.toTxt;

public class TestInstance {
	Format fact;
	
	@Before
	public void setUp() throws Exception {
	fact = new toTxt();
	}

	@Test
	public void testCreateFactory() {
		assertTrue(fact instanceof toTxt);
	}

}
