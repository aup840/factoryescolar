package test;

import static org.junit.Assert.*;
import java.io.File;
import java.util.ArrayList;
import org.junit.Before;
import org.junit.Test; 
import fwk.Alumno;
import fwk.BaseDatos;

public class PruebaTXT {
	BaseDatos bd;
	
	@Before
	public void setUp() throws Exception {
		bd = new BaseDatos();
		bd.cargar();
		ArrayList<Alumno> alumnos = bd.getAlumnos();	
	}

	@Test
	public void testExportar() {
		File archivo = new File("resultados.txt");
		assertTrue(archivo.exists());
	}
}